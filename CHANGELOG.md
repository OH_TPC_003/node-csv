## 2.0.0

- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs语法适配

## 1.0.1
示例适配API 9 Stage模型。

## 1.0.0
1.支持 CSV 字符串和 Javascript 对象的灵活生成器。

2.支持将 CSV 文本转换为数组或对象的解析器。

3.支持将记录转换为 CSV 文本的字符串化器。

4.支持简单对象转换框架，扩展原生 Node.js转换流 API。