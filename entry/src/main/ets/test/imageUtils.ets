/**
 *  MIT License
 *
 *  Copyright (c) 2023 Huawei Device Co., Ltd.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */
import image from '@ohos.multimedia.image';

export function getPixelMap(buffer: ArrayBuffer, width: number, height: number): Promise<PixelMap> {
  return new Promise<PixelMap>((resolve, reject) => {
    interface defaultSizeType {
      "height": number,
      "width": number
    }

    let defaultSize:defaultSizeType = {
      "height": width,
      "width": height
    };

    let initializationOpts:image.InitializationOptions = {
      alphaType: 0,
      scaleMode: 1,
      editable: true,
      size: defaultSize,
      pixelFormat: 4,
    };

    // 根据二维码图片数据创建PixelMap
    image.createPixelMap(buffer, initializationOpts, (err, data) => {
      if (err) {
        console.error(TAG + 'getPixelMap error code is  ' + err.code);
        console.error(TAG + 'getPixelMap error msg is ' + err.message);
      } else {
        // 读取新创建的PixelMap
        let pixelBytesNumber = data.getPixelBytesNumber()
        console.error(TAG + 'getPixelMap pixelBytesNumber ' + pixelBytesNumber);
        console.error(TAG + 'getPixelMap Succeed');
        resolve(data)
      }
    });
  })
}


const  TAG = "imageUtils-----------------------";